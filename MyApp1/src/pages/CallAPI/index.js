import React, {useEffect, useState} from 'react';
import {Button, StyleSheet, Text, View, Image} from 'react-native';

const CallAPI = () => {
    const [dataUser, setdataUser] = useState({
        avatar:'',
        email:'',
        first_name:'',
        last_name:'',
    });

    const [dataJob, setdataJob] = useState({
        job:'',
        name: '',
    });
    useEffect(() => {
        // Call API dengan Method GET
//   fetch('https://reqres.in/api/users/2')
//   .then(response => response.json())
//   .then(json => console.log(json))

//   // Call API dengan Method POST
//   const dataForAPI = {
//     "name": "morpheus",
//     "job": "leader"
//   }
//   console.log('data object', dataForAPI);
//   console.log('data stringfy', JSON.stringify(dataForAPI))
//   fetch('https://reqres.in/api/users', {
//       method: 'POST',
//       headers: {
//           'Content-Type': 'application/json'
//       },
//       body: JSON.stringify(dataForAPI) 
//   })

//   .then(response => response.json())
//   .then(json => {
//       console.log('Post Response',json)
//   })


    }, []);

    const getData = () => {
  fetch('https://reqres.in/api/users/2')
  .then(response => response.json())
  .then(json => {
      console.log(json)
  setdataUser(json.data)
    })
}

const postData = () => {
    const dataForAPI = {
            "name": "morpheus",
            "job": "leader"
          }

          fetch('https://reqres.in/api/users', {
              method: 'POST',
              headers: {
                  'Content-Type': 'application/json'
              },
              body: JSON.stringify(dataForAPI) 
          })
        
          .then(response => response.json())
          .then(json => {
              console.log('Post Response',json)
              setdataJob(json)
          })
}
  return (
    <View style={styles.container}>
      <Text style={styles.textTittle}>Call Fake Api</Text>
      <Button title="GET DATA" onPress={getData} />
      <Text>Response GET DATA</Text>
      <Image source={{uri: dataUser.avatar}} style={styles.avatar} />
      <Text>{dataUser.first_name}</Text>
      <Text>{dataUser.last_name}</Text>
      <Text>{dataUser.email}</Text>
      <View style={styles.line} />
      <Button title="POST DATA" onPress={postData}/>
      <Text>Response Data</Text>
      <Text>{dataJob.name}</Text>
      <Text>{dataJob.job}</Text>
    </View>
  );
};

export default CallAPI;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  textTittle: {
    textAlign: 'center',
  },
  line: {
      height: 2, backgroundColor:'black', marginVertical: 20
  },
  avatar: {
      width:100, height:100, borderRadius:100,
  }
});
