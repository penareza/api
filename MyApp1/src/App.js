import React from 'react';
import {ScrollView, View} from 'react-native';
import CallAPI from './pages/CallAPI';

const App = () => {
  return (
    <View>
      <ScrollView>
        <CallAPI />
      </ScrollView>
    </View>
  );
};

export default App;
