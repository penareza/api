import React from 'react';
import {ScrollView, View} from 'react-native';
import CallapiAxios from '../src/pages/CallapiAxios'
import LocalAPI from './pages/LocalAPI';

const App = () => {
  return (
    <View>
      <ScrollView>
        {/* <CallapiAxios /> */}
        <LocalAPI />
      </ScrollView>
    </View>
  );
};

export default App;
