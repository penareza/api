import Axios from 'axios'
import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, TextInput, View, Button, Image, ScrollView, TouchableOpacity, Alert } from 'react-native'

const Item = ({Name, Email, Work, Angkatan, onPress, onDelete}) => {
    return (
    <View style={styles.itemContainer}>
        <Image source={{uri:'https://yt3.ggpht.com/a/AATXAJy5LLCs-DPIxCnBLN8vcAYGVf7b7Mw8lagUA5aK8w=s88-c-k-c0x00ffffff-no-rj'}} style={styles.avatar}/>
        <View style={styles.desc}>
            <TouchableOpacity onPress={onPress}>
                <Text style={styles.descName}>{Name} </Text>
            </TouchableOpacity>
            <Text style={styles.descEmail}>{Email} </Text>
            <Text style={styles.descWork}>{Work} </Text>
            <Text style={styles.descAngkatan}>{Angkatan} </Text>
        </View>
        <TouchableOpacity onPress={onDelete}>
            <Text style={styles.deleted}>X</Text>
        </TouchableOpacity>
        
    </View>
    )
}

const LocalAPI = () => {
    const [Name, setName] = useState("");
    const [Email, setEmail] = useState("");
    const [Work, setWork] = useState("");
    const [Angkatan, setAngkatan] = useState("");
    const [Alumnus, setAlumnus] =useState([]);

    const [button, setButton] = useState("Simpan");
    const [selectedAlumni, setSelectedAlumni] = useState({})
    


    // Proses GetData 
    useEffect(() => {
        getData();
    }, []);

    const submit = () => {
        const data = {
            Name: Name,
            Email: Email,
            Work: Work,
            Angkatan: Angkatan,
        }

        // Post Data
        if(button === 'Simpan')
        {

            Axios.post('http://10.0.2.2:3004/alumni', data)
            .then(res => {
            console.log('res', res);
            setName("");
            setEmail("");
            setWork("");
            setAngkatan("");
            getData();
        })

        } else if(button === 'Update')
        {
            Axios.put(`http://10.0.2.2:3004/alumni/${selectedAlumni.id}`, data)
            .then(res => {
                console.log('res update', res);
                
                setName("");
                setEmail("");
                setWork("");
                setAngkatan("");
                getData();
                setButton("Simpan");

            })
        }
        
    }

    // Get data 

    const getData = () => {
        Axios.get('http://10.0.2.2:3004/alumni')
        .then(res => {
            console.log('res Get Data', res)
            setAlumnus(res.data);
        })
    }

    const selectItem = (item) => {
        console.log('selected' , item);
        setSelectedAlumni(item);
        setName(item.Name);
        setEmail(item.Email);
        setWork(item.Work);
        setAngkatan(item.Angkatan);
        setButton("Update");
    }

    const deleteItem = (item) => {
        console.log(item);
        Axios.delete(`http://10.0.2.2:3004/alumni/${item.id}`)
        .then(res => {
            console.log('res: delete', res)
            getData();


        })
    }

    return (
        
        <View style={styles.container}>
            <ScrollView>
            <Text style={styles.textTittle}>Local API (Json Server)</Text>
            <Text style={{textAlign:'center', marginBottom:10,}}>Masukan Anggota IKA-DIPA SULTRA</Text>
            <TextInput placeholder='Nama Lengkap'  style={styles.input} value={Name} onChangeText={(value) => setName(value)}/>
            <TextInput placeholder='Email' style={styles.input} value={Email} onChangeText={(value) => setEmail(value)}/>
            <TextInput placeholder='Pekerjaan' style={styles.input} value={Work} onChangeText={(value) => setWork(value)}/>
            <TextInput placeholder='Angkatan' style={styles.input} value={Angkatan} onChangeText={(value) => setAngkatan(value)} />
            <Button title = {button} onPress={submit}  style={styles.input} />
        <View style={styles.line}/>

            {Alumnus.map(alumni => {
                return <Item 
                key={alumni.id} 
                Name={alumni.Name} 
                Email={alumni.Email} 
                Work={alumni.Work} 
                Angkatan={alumni.Angkatan} 
                onPress={() => selectItem(alumni)}
                onDelete={() => Alert.alert(
                    'Peringatan', 
                    'Anda Yakin Ingin Menghapus data ini?',
                    [
                        {
                            text: 'Tidak',
                            onPress: () => console.log('Button Tidak')
                        },
                        {
                            text: 'Ya',
                            onPress: () => deleteItem(alumni)
                        }
                    ])}/>
            })}
    

            </ScrollView>

        </View>

    )
}

export default LocalAPI

const styles = StyleSheet.create({
    container : 
    {
        padding:20,
    },
    textTittle:
    {
        textAlign:'center',
    },
    input: {
        borderWidth:1, marginBottom:12, borderRadius:25,
        paddingHorizontal:18,
    },
    line:
    {backgroundColor:'black', height:2, marginVertical:20,},

    avatar:
    {
        width:80, height:80, borderRadius:100,
    },

    itemContainer:
    {
        flexDirection: 'row', marginBottom:10,
    },

    desc:{
        marginLeft:18, flex: 1,
    },

    descName: {
        fontSize: 20, fontWeight:'bold',
    },
    descMail: {
        fontSize: 16, marginTop: 8,
    },
    descWork:{
        fontSize: 16, marginTop: 8,
    },

    descAngkatan: {
        fontSize: 12, marginTop: 8,
    },

    deleted:
    {
        fontSize: 20, fontWeight:'bold', color:'red',
    }

     
})
