import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Button, StyleSheet, Text, View, Image} from 'react-native';

const CallapiAxios= () => {
    
    
    
    const [dataUser, setdataUser] = useState({
        avatar:'',
        email:'',
        first_name:'',
        last_name:'',
    });

    const [dataJob, setdataJob] = useState({
        job:'',
        name: '',
    });





const getData = () => {
Axios.get('https://reqres.in/api/users/3')
.then(result => {
    setdataUser(result.data.data)
})
.catch(err => console.log('err:', err))
}

const postData = () => {
    const dataForAPI = {
            "name": "morpheus",
            "job": "leader"
          }
          Axios.post('https://reqres.in/api/users', dataForAPI)
          .then(result => {
              setdataJob(result.data)
          })
}
  return (
    <View style={styles.container}>
      <Text style={styles.textTittle}>Calling API dengan Axios</Text>
      <Button title="GET DATA" onPress={getData} />
      <Text>Response GET DATA</Text>
      {dataUser.avatar.length > 0 && (
          <Image source={{uri: dataUser.avatar}} style={styles.avatar} />
      )}
      
      <Text>{dataUser.first_name}</Text>
      <Text>{dataUser.last_name}</Text>
      <Text>{dataUser.email}</Text>
      <View style={styles.line} />
      <Button title="POST DATA" onPress={postData}/>
      <Text>Response Data</Text>
      <Text>{dataJob.name}</Text>
      <Text>{dataJob.job}</Text>
    </View>
  );
};

export default CallapiAxios;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  textTittle: {
    textAlign: 'center',
  },
  line: {
      height: 2, backgroundColor:'black', marginVertical: 20
  },
  avatar: {
      width:100, height:100, borderRadius:100,
  }
});
